# dodge-the-creeps

## Rules

- To move the player, touch the screen at the desired position
- Dodge the creeps by moving around

## Notes

- The web version does not work with Microsoft Edge

## Credits

- https://docs.godotengine.org/en/3.1/getting_started/step_by_step/your_first_game.html
